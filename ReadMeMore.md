# Group 과 SubGroup

## Group

**결론부터 말하면 gitLab에서 group은 repository(gitLab에서는 'project'라고 표현하는데, 똑 같은 말이다. 혹시 두 단어와 더불어 '저장소'라는 단어까지  뒤에 혼용되더라도 똑 같은 것이라는 걸 인식하길...)가 아니다.** group은 단순히 repository들이 담겨있는 'container'일 뿐이다. 따라서 clone, add, commit, push 등 모든 git명령이 안 먹는다. 단지 여러 개의 repository들을 따로 관리한다는 점에서 편리하지만, group과 헛갈릴 경우 애 먹을 수 있다.

다시 말하지만 group은 repository(s) 를 담아두는 보따리 일 뿐이고 중요한 것은 '어느 위치'에 group과 repository를 만드느냐 하는 점이다.

## group 만드는 방법

1. 자신의 group이 만들어질 위치(우리가 흔히 윈도우 탐색기에서 보는 폴더들의 상하위 관계에서의 위치다.)로 이동한다.

2. 'New project' 버튼을 클릭한다.

3. Project 이름 입력란 아래에 'Create a group'이라는 파란색 링크를 클릭하면 이동되는 페이지ㅣ에서 group을 만들면 된다.

## Group 안에 subGroup 만들기 

gitLab에서는 group 속의 group, 즉 subgroup을 만들 수 있다. gitLab에서 만든 그룹을 선택하면 오른쪽에 'New Project' 버튼이 있는데, 버튼 왼쪽에 드롭다운할 수 있는 화살표가 보인다.

화살표를 드롭다운하여 'New subgroup' 항목을 클릭하면 group 속에 subgroup을 만들 수 있다. 이러한 그룹들을 어떻게 관리하느냐는 물론 개인의 몫이다.

## group과 repository의 개념

이 개념은 'gourp은 윈도우 탐색기에서 폴더', 'repository는 파일'과 동일하다 생각한다. 다만, 어떤 종류의 확장자를 갖는 단순한 1 개의 파일이 아닌 속에 여러개의 파일이 압축된 .zip, .tar, .rar 등의'압축파일'들이라 생각하면 개념을 얼추 비슷하게 잡을 것이라 '생각'한다.


## group과 repository를 구분하는 방법

많은 group과 repository들이 생기면, 아니 몇 개 안되더라도 처음 gitLab을 이용하면 당장 헷갈릴 수 있다. 

* group은 이름 왼쪽에 폴더 아이콘이

* repository는 외쪽에 ribbon 아이콘이

표시되므로 구분 가능하다.

## gitLab의 불편성

gitLab은 선발주자인 github나 Bitbucket을 따라가기 위해 저장공간 10GB를 제공하고 group과 같은 편리성(익숙해지기 전에는 사실 편리성이라기도 뭐 하지만...)도 제공하지만, '결정적'으로 git을 GUI로 운용하게 해주는  [SourceTree](https://www.sourcetreeapp.com/)가 gitLab을 지워하지 않는다는 점이다.

하지만 걱정할 필요는 없다. 내 경우 git을 처음 배울 때 sourceTree가 뭔지도 모르고 필요하다길래 설치했었지만, 지금은 git bash ternimal에서 직접 명령하는게 훨씬 편할 뿐더러, sourceTree가 되려 헛갈려서 안 쓰고 삭제했다. 참고로 [git hosting service를 제공하는 대표적인 3개업체 에 대한 문서](https://m.blog.naver.com/dlwhdgur20/221006619001)를 참조할 것.

## gitLab에서 못 쓰는 SourceTree를 대체하는 방법

이 방법은 VSCode[Visual Studio Code](https://code.visualstudio.com/)사용하는 사람에 국한된 것이기는 하지만 VSCode extension 중 [gitLens](https://gitlens.amod.io/)를 사용하는 것이다. 이 확장 사용법 대해서는 설명하지 않는다.

## group관 repository의 운용(運用)

group과 repository의 운용은 한 마디로 탐색기에서 폴더와 파일을 만드는 이치와 동일하다. 따라서 각자의 몫이다. 즉, 자신의 하드디스크에 폴더를 만들어야 할 때와 파일을 만들어야 할 때를 구분할 수 있다면 이미 능력은 구비한 것이다.

하지만 gitLab에 만든 group과 repository의 위계구조(hierarchical structure)를 자신의 하드디스크에도 만들 것을 강력히 권장한다.

SourceTree를 지원받지 못하는 gitLab에서 이 위계구조를 보려면 위에서 추천한 VSCode extension인 [gitLens](https://gitlens.amod.io/)의 도움을 받으면 헷갈리지 않고 gitLab에서의 위계구조를 보며 자신의 하드디스크에 gitLab에서 만든 위계구조와 동일한 구조로 폴더를 생성할 수 있을 것이다.

* 끝으로 이런 문서들이 대개 그러하듯, 내용을 읽고 이해했다는 것과 똑 같이 실현할 수 있는 것은 다른 문제다. 단언컨데 실습을 하지 않는다면 한 걸음 나아가는 것도 곤란할 것이다.

수 많은 시행착오를 거쳐 자신의 무의식 속에 경험이 기록될 때 비로소 자신의 것이 될 것이다.